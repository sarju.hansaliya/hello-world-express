const express = require('express');
const app = express();
const CronJob = require('cron').CronJob;
const fetch = require('node-fetch');
const cheerio = require('cheerio');
const nodemailer = require('nodemailer');
const TinyURL = require('tinyurl');
const sendinBlue = require('nodemailer-sendinblue-transport');
const port = 8080;

const transporter = nodemailer.createTransport(
  sendinBlue({ apiKey: '7hjmUsDGkLc8gyd3' })
);

app.get('/', (req, res) => res.send('Hello World!'));

app.listen(port, () =>
  console.log(`Example app listening on port:::  ${port}!`)
);

new CronJob(
  '0 20 * * *',
  async function() {
    await func();
    console.log('Email Sent', new Date());
  },
  null,
  true,
  'Asia/Calcutta'
);

const func = async () => {
  let list = [];
  let htmlStr = '';
  const res = await fetch('https://world4ufree.host', { method: 'GET' });
  const body = await res.text();
  let $ = cheerio.load(body);
  $('#loop')
    .children()
    .each(async (i, ele) => {
      const title = $(ele)
        .find('h2 a')
        .attr('title');
      const href = $(ele)
        .find('h2 a')
        .attr('href');
      if (title) {
        const link = await TinyURL.shorten(href);
        htmlStr += `
          <div>
            <a href="${link}">${title}</a>
          </div>
        `;
      }
    });

  const regesPage2 = await fetch('https://world4ufree.host/page/2/', {
    method: 'GET',
  });
  const bodyPage2 = await regesPage2.text();
  $ = cheerio.load(bodyPage2);
  $('#loop')
    .children()
    .each(async (i, ele) => {
      const title = $(ele)
        .find('h2 a')
        .attr('title');
      const href = $(ele)
        .find('h2 a')
        .attr('href');
      if (title) {
        const link = await TinyURL.shorten(href);
        htmlStr += `
          <div>
            <a href="${link}">${title}</a>
          </div>
        `;
      }
    });
  let info = await transporter.sendMail({
    from: 'Sarah Walker <sarahwalker1006@gmail.com>', // sender address
    to: 'sarju.hansaliya@gmail.com, niravrpatel17@gmail.com', // list of receivers
    subject: 'Today world4u.host', // Subject line
    text: htmlStr, // plain text body
    html: `${htmlStr}`, // html body
  });
};
func();

module.exports = app;
